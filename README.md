# README #

This is the Wordpress installation for the Cat Cafe Melbourne.

##Theme

The site is displayed using the `wp_cat_cafe` theme which has been created especially for this site.

##Plugins

###Custom Plugins

####cat_cafe_cat_type

This is a custom post type plugin which allows for the addition of cats to the website. 

A cat post type contains the following:

* Name of the cat
* Description of the cat
* Image of the cat

####cat_cafe_faq_type

This is a custom post type plugin which allows for the addition of frequently asked questions to the website.

A faq post type contains the following:

* A frequently asked question
* An answer

###Third Party Plugins

####Custom Metaboxes 2 (CMB2)

Custom Metaboxes is a plugin which can be used by developers to create rich metaboxes. It is used to add custom fields to theme templates and custom plugins settings pages. 